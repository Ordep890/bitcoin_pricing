#test_bitcoin_pricing_app.py
import os, sys
import unittest
SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(SCRIPT_DIR))
from unittest.mock import patch, Mock
from src.bitcoin_pricing_app import get_tweleve_data_api_key, get_bitcoin_pricing_from_difference_currencies, summary_of_bitcoin_pricing_in_different_currencies    



class TestBitcoinPricingApp(unittest.TestCase):

    @patch('os.environ.get')
    def test_get_tweleve_data_api_key(self, mock_os_environ_get):
        """Test get_tweleve_data_api_key()"""
        mock_os_environ_get.return_value = '123456789'
        self.assertEqual(get_tweleve_data_api_key(), '123456789')
    
    @patch('os.environ.get')
    def test_get_tweleve_data_api_key_request_error(self, mock_os_environ_get):
        """Test get_tweleve_data_api_key() request error"""
        mock_os_environ_get.side_effect = Exception('Error')
        self.assertEqual(get_tweleve_data_api_key(), '0000000000000000000')


if __name__ == '__main__':
    unittest.main()