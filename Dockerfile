# syntax = docker/dockerfile:1
FROM python:3.10.13-alpine3.18
COPY /src /src
RUN pip install requests
CMD ["python", "/src/bitcoin_pricing_app.py"]